<?php get_header(); ?>

	<!-- START_FILTER_SIDASH -->
	<section class="filterContent">
		<div class="promoBlock__sidSonderangebote">
			<hr>
			<h4>Filter</h4>
		</div>
		<div class="clearfix sid_space"></div>
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<div class="filterContent__img">
						<div class="filterContent__img-car filterContent__img-bmw">
							<span>BMW</span>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<div class="filterContent__img">
						<div class="filterContent__img-car filterContent__img-cadillac">
							<span>Cadillac</span>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<div class="filterContent__img">
						<div class="filterContent__img-car filterContent__img-ferrari">
							<span>Ferrari</span>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<div class="filterContent__img">
						<div class="filterContent__img-car filterContent__img-italdesign">
							<span>Italdesign</span>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<div class="filterContent__img">
						<div class="filterContent__img-car filterContent__img-lamborghini">
							<span>Lamborghini</span>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<div class="filterContent__img">
						<div class="filterContent__img-car filterContent__img-landrover">
							<span>Land Rover</span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<div class="filterContent__img">
						<div class="filterContent__img-car filterContent__img-lexus">
							<span>Lexus</span>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<div class="filterContent__img">
						<div class="filterContent__img-car filterContent__img-maybach">
							<span>Maybach</span>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<div class="filterContent__img">
						<div class="filterContent__img-car filterContent__img-mclaren">
							<span>McLaren</span>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<div class="filterContent__img">
						<div class="filterContent__img-car filterContent__img-merc">
							<span>Mercedes-Benz</span>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<div class="filterContent__img">
						<div class="filterContent__img-car filterContent__img-porsche">
							<span>Porsche</span>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
					<div class="filterContent__img">
						<div class="filterContent__img-car filterContent__img-tesla">
							<span>Tesla</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="promoBlock__sidSonderangebote cae">
			<hr>
			<div class="container">
				<div class="row two_filter">
					<div class="col-md-2">
						<div class="select">
							<select name="Modell" id="Modell">
								<option value="0">Modell</option>
							</select>
						</div>
					</div>
					<div class="col-md-1">
					</div>
					<div class="col-md-2">
						<div class="select">
							<select name="Kraftstoff" id="Kraftstoff">
								<option value="" disabled selected="selected">Kraftstoff</option>
								<option value="Benzin">Benzin</option>
								<option value="Diesel">Diesel</option>
								<option value="Elekro">Elekro</option>
								<option value="Hybrid">Hybrid</option>
							</select>
						</div>
					</div>
					<div class="col-md-1">
					</div>
					<div class="col-md-2">
						<label for="getpanzert">Getpanzert <input type="checkbox" id="getpanzert"></label>
					</div>
					<button id="filter_two">Übernehmen</button>
				</div>


			</div>
		</div>
	</section>
	<!-- END_FILTER_SIDASH -->
	<div class="sid_space"></div>
	<!-- START_PRODUCT_GRID_SIDASH -->
	<section class="prodGrid">
		<div class="container">
			<div class="row">

				<div class="col-lg-3 col-md-3 col-sm-12 BMW Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17312_1.360x240.jpg"
												alt="BMW i8 PURE IMPULSE*20&quot;*HEAD-UP*H.Kardon*LED">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>BMW i8 PURE IMPULSE*20"*HEAD-UP*H.Kardon*LED</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>170 kW (231 PS)</span></p>
								<p>km-Stand:<span>3.300</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 2.1l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>49 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- A+ -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>75.900 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>90.321 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17312_bmw-i8-pure-impulse-20-head-up-h-kardon-led">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 BMW Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17235_1.360x240.jpg"
												alt="BMW M5 FIRST EDITION*1of 400*Ceramic*FULL OPTIONS">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>BMW M5 FIRST EDITION*1of 400*Ceramic*FULL OPTIONS</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>441 kW (600 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 10.5l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>241 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- G -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>139.900 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>166.481 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17235_bmw-m5-first-edition-1of-400-ceramic-full-options">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Cadillac Benzin Gepanzert">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17244_1.360x240.jpg"
												alt="Cadillac Escalade ESV LONG*ARMORED B6*STOCK">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Cadillac Escalade ESV LONG*ARMORED B6*STOCK</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>313 kW (426 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 12.6l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>287 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- E -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>127.000 €</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17244_cadillac-escalade-esv-long-armored-b6-stock">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Ferrari Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17050_1.360x240.jpg"
												alt="Ferrari 488 Spider *Ext Carbon*Bicolor*Electric seats">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Ferrari 488 Spider *Ext Carbon*Bicolor*Electric seats</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>492 kW (669 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 8.4l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>260 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- G -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>254.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>302.260 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17050_ferrari-488-spider-ext-carbon-bicolor-electric-seats">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 GMC Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<h3>NO IMAGE</h3>
						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>GMC Yukon Denali XL </h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>309 kW (420 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 0l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>0 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!--  -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>74.000 €</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17407_gmc-yukon-denali-xl">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Italdesign Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17158_1.360x240.jpg"
												alt="Italdesign Zerouno Limited Edition - only 5 units! Number 2!">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Italdesign Zerouno Limited Edition - only 5 units! Number 2!</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>449 kW (610 PS)</span></p>
								<p>km-Stand:<span>50</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 0l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>0 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- B -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>2.200.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>2.618.000 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17158_italdesign-zerouno-limited-edition-only-5-units-number-2">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 LAMBORGHINI Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/16011_1.360x240.jpg"
												alt="LAMBORGHINI Aventador LP 750-4 Superveloce Carbon Paket/Lift">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>LAMBORGHINI Aventador LP 750-4 Superveloce Carbon Paket/Lift</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>552 kW (751 PS)</span></p>
								<p>km-Stand:<span>50</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 16l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>370 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- D -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>379.900 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>452.081 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/16011_lamborghini-aventador-lp-750-4-superveloce-carbon-paket-lift">Weitere
									Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Land-Rover Benzin Gepanzert">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17392_1.360x240.jpg"
												alt="Land Rover Range Rover 5.0 SC*MY19 *ARMORED B6 *IN STOCK">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Land Rover Range Rover 5.0 SC*MY19 *ARMORED B6 *IN STOCK</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>381 kW (518 PS)</span></p>
								<p>km-Stand:<span>100</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 13l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>305 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- F -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>183.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>217.770 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17392_land-rover-range-rover-5-0-sc-my19-armored-b6-in-stock">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Land-Rover Diesel">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17082_1.360x240.jpg"
												alt="Land Rover Range Rover Velar 3.0d R-Dynamic S*R20*Panorama">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Land Rover Range Rover Velar 3.0d R-Dynamic S*R20*Panorama</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>221 kW (300 PS)</span></p>
								<p>km-Stand:<span>35</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Diesel | 6.4l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>167 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- B -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>63.900 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>76.041 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17082_land-rover-range-rover-velar-3-0d-r-dynamic-s-r20-panorama">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Land-Rover Diesel">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17183_1.360x240.jpg"
												alt="Land Rover Range Rover Velar D240 R-Dynamic S*R20*Panorama">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Land Rover Range Rover Velar D240 R-Dynamic S*R20*Panorama</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>177 kW (240 PS)</span></p>
								<p>km-Stand:<span>40</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Diesel | 5.8l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>154 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- B -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>57.900 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>68.901 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17183_land-rover-range-rover-velar-d240-r-dynamic-s-r20-panorama">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Land-Rover Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17128_1.360x240.jpg"
												alt="Land Rover Range Rover Velar P250 2.0 Petrol R-Dynamic S">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Land Rover Range Rover Velar P250 2.0 Petrol R-Dynamic S</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>184 kW (250 PS)</span></p>
								<p>km-Stand:<span>30</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 7.6l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>173 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- C -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>59.500 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>70.805 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17128_land-rover-range-rover-velar-p250-2-0-petrol-r-dynamic-s">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Land-Rover Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17366_1.360x240.jpg"
												alt="Land Rover Range Rover Velar S 2.0 Petrol*Panoram*TFT*R19 ">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Land Rover Range Rover Velar S 2.0 Petrol*Panoram*TFT*R19 </h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>184 kW (250 PS)</span></p>
								<p>km-Stand:<span>38</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 7.6l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>173 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- E -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>50.500 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>60.095 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17366_land-rover-range-rover-velar-s-2-0-petrol-panoram-tft-r19">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Lexus Benzin Gepanzert">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17209_1.360x240.jpg"
												alt="Lexus LX 570 Luxury*ARMORED B6*4-seats VIP INTERIOR">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Lexus LX 570 Luxury*ARMORED B6*4-seats VIP INTERIOR</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>280 kW (381 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 12.9l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>403 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- G -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>170.000 €</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17209_lexus-lx-570-luxury-armored-b6-4-seats-vip-interior">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Lexus Benzin Gepanzert">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17187_1.360x240.jpg"
												alt="Lexus LX 570 Luxury*ARMORED B6*FULL OPTIONS">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Lexus LX 570 Luxury*ARMORED B6*FULL OPTIONS</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>280 kW (381 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 12.9l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>403 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- G -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>149.000 €</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17187_lexus-lx-570-luxury-armored-b6-full-options">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Maybach Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17229_1.360x240.jpg"
												alt="Maybach  Mercedes S560 4M*4 Seats*Panorama*Exclusive">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Maybach Mercedes S560 4M*4 Seats*Panorama*Exclusive</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>345 kW (469 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 9.3l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>209 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- C -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>155.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>184.450 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17229_maybach-mercedes-s560-4m-4-seats-panorama-exclusive">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Maybach Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17404_1.360x240.jpg"
												alt="Maybach  Mercedes S560 4M*4 Seats*Panorama*Exclusive*R20">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Maybach Mercedes S560 4M*4 Seats*Panorama*Exclusive*R20</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>345 kW (469 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 9.3l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>209 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- C -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>153.500 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>182.665 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17404_maybach-mercedes-s560-4m-4-seats-panorama-exclusive-r20">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Maybach Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17367_1.360x240.jpg"
												alt="Maybach  Mercedes S650 4 Seats*Panorama*Burmester*R20">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Maybach Mercedes S650 4 Seats*Panorama*Burmester*R20</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>463 kW (629 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 9.3l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>289 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- D -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>177.900 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>211.701 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17367_maybach-mercedes-s650-4-seats-panorama-burmester-r20">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Maybach Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17224_1.360x240.jpg"
												alt="Maybach  Mercedes S650 Cabrio*Limited Edition 1/300">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Maybach Mercedes S650 Cabrio*Limited Edition 1/300</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>463 kW (630 PS)</span></p>
								<p>km-Stand:<span>0</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 11.9l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>272 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- F -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>265.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>315.350 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17224_maybach-mercedes-s650-cabrio-limited-edition-1-300">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Maybach Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17164_1.360x240.jpg"
												alt="Maybach  Mercedes-Maybach S650 Cabrio 1 of 300 ">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Maybach Mercedes-Maybach S650 Cabrio 1 of 300 </h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>463 kW (630 PS)</span></p>
								<p>km-Stand:<span>50</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 12.7l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>289 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- F -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>295.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>351.050 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17164_maybach-mercedes-maybach-s650-cabrio-1-of-300">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 McLaren Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17093_1.360x240.jpg"
												alt="McLaren 720S Coupe Ext/Int Carbon*Stealth*Bower&Wilkins">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>McLaren 720S Coupe Ext/Int Carbon*Stealth*Bower&Wilkins</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>530 kW (720 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 10.7l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>249 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- F -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>265.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>315.350 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17093_mclaren-720s-coupe-ext-int-carbon-stealth-bower-wilkins">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Diesel">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17177_1.360x240.jpg"
												alt="Mercedes-Benz E 220 d 4M AMG*Panorama*R20*MY18">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz E 220 d 4M AMG*Panorama*R20*MY18</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>143 kW (194 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Diesel | 4.9l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>129 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- A+ -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>49.900 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>59.381 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17177_mercedes-benz-e-220-d-4m-amg-panorama-r20-my18">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Diesel">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17276_1.360x240.jpg"
												alt="Mercedes-Benz G 350 d * PROFESSIONAL*LIMITED EDITION*1 of 463">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz G 350 d * PROFESSIONAL*LIMITED EDITION*1 of 463</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>180 kW (245 PS)</span></p>
								<p>km-Stand:<span>50</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Diesel | 9.9l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>261 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- B -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>87.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>103.530 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17276_mercedes-benz-g-350-d-professional-limited-edition-1-of-463">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17308_1.360x240.jpg"
												alt="Mercedes-Benz G 500 4x4^2 BRABUS*550 PS*Sport Exhaust*Side Steps ">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz G 500 4x4^2 BRABUS*550 PS*Sport Exhaust*Side Steps </h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>405 kW (550 PS)</span></p>
								<p>km-Stand:<span>50</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 13.8l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>323 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- E -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>239.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>284.410 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17308_mercedes-benz-g-500-4x4-2-brabus-550-ps-sport-exhaust-side-steps">Weitere
									Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17026_1.360x240.jpg"
												alt="Mercedes-Benz G 500 Cabrio BRABUS WIDESTAR*EXHAUST*R22">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz G 500 Cabrio BRABUS WIDESTAR*EXHAUST*R22</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>285 kW (387 PS)</span></p>
								<p>km-Stand:<span>7.980</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 14.9l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>348 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- G -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>205.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>243.950 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17026_mercedes-benz-g-500-cabrio-brabus-widestar-exhaust-r22">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Benzin Gepanzert">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17217_1.360x240.jpg"
												alt="Mercedes-Benz G 63 AMG ARMORED B6*EXCLUSIVE*FULL Options">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz G 63 AMG ARMORED B6*EXCLUSIVE*FULL Options</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>419 kW (570 PS)</span></p>
								<p>km-Stand:<span>50</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 13.8l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>322 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- G -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>208.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>247.520 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17217_mercedes-benz-g-63-amg-armored-b6-exclusive-full-options">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17063_1.360x240.jpg"
												alt="Mercedes-Benz G 63 AMG BRABUS*WIDESTAR*R23*Ride Control*NP190">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz G 63 AMG BRABUS*WIDESTAR*R23*Ride Control*NP190</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>419 kW (570 PS)</span></p>
								<p>km-Stand:<span>7.500</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 13.8l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>322 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- G -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>122.900 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>146.251 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17063_mercedes-benz-g-63-amg-brabus-widestar-r23-ride-control-np190">Weitere
									Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Diesel">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17297_1.360x240.jpg"
												alt="Mercedes-Benz GLC 250 d 4M Coupe/AMG/20&quot;/Schiebedach/KeylessGo">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz GLC 250 d 4M Coupe/AMG/20"/Schiebedach/KeylessGo</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>150 kW (204 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Diesel | 5.4l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>143 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- A -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>52.900 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>62.951 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17297_mercedes-benz-glc-250-d-4m-coupe-amg-20-schiebedach-keylessgo">Weitere
									Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Diesel">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17298_1.360x240.jpg"
												alt="Mercedes-Benz GLC 250 d 4M Coupe/AMG/20&quot;/Schiebedach/KeylessGo">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz GLC 250 d 4M Coupe/AMG/20"/Schiebedach/KeylessGo</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>150 kW (204 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Diesel | 5.4l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>143 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- A -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>53.900 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>64.141 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17298_mercedes-benz-glc-250-d-4m-coupe-amg-20-schiebedach-keylessgo">Weitere
									Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17306_1.360x240.jpg"
												alt="Mercedes-Benz GLC 63 AMG 4M Coupe/EDITION 1/21&quot;/CerBrake/HUD">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz GLC 63 AMG 4M Coupe/EDITION 1/21"/CerBrake/HUD</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>375 kW (510 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 10.7l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>244 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- F -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>108.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>128.520 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17306_mercedes-benz-glc-63-amg-4m-coupe-edition-1-21-cerbrake-hud">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17307_1.360x240.jpg"
												alt="Mercedes-Benz GLC 63 AMG 4M Coupe/EDITION 1/21&quot;/Sunroof/HUD">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz GLC 63 AMG 4M Coupe/EDITION 1/21"/Sunroof/HUD</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>375 kW (510 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 10.7l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>244 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- F -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>104.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>123.760 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17307_mercedes-benz-glc-63-amg-4m-coupe-edition-1-21-sunroof-hud">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17119_1.360x240.jpg" alt="Mercedes-Benz GLS 63 AMG BRABUS">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz GLS 63 AMG BRABUS</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>430 kW (585 PS)</span></p>
								<p>km-Stand:<span>50</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 12.3l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>288 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- G -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>147.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>174.930 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17119_mercedes-benz-gls-63-amg-brabus">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17278_1.360x240.jpg"
												alt="Mercedes-Benz S 560 L 4M*MY18*Designo*AMG*4-Seats*R20*Panorama">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz S 560 L 4M*MY18*Designo*AMG*4-Seats*R20*Panorama</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>345 kW (469 PS)</span></p>
								<p>km-Stand:<span>0</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 8.8l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>200 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- C -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>136.500 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>162.435 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17278_mercedes-benz-s-560-l-4m-my18-designo-amg-4-seats-r20-panorama">Weitere
									Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17403_1.360x240.jpg"
												alt="Mercedes-Benz S 560 Maybach 4M*4 Seats*Panorama*Exclusive*R20">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz S 560 Maybach 4M*4 Seats*Panorama*Exclusive*R20</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>345 kW (469 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 9.3l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>209 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- C -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>151.500 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>180.285 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17403_mercedes-benz-s-560-maybach-4m-4-seats-panorama-exclusive-r20">Weitere
									Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17195_1.360x240.jpg"
												alt="Mercedes-Benz S 560 Maybach 4M*4 Sitze*Panorama*Exklusive*R20">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz S 560 Maybach 4M*4 Sitze*Panorama*Exklusive*R20</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>345 kW (469 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 9.3l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>209 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- C -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>155.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>184.450 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17195_mercedes-benz-s-560-maybach-4m-4-sitze-panorama-exklusive-r20">Weitere
									Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17401_1.360x240.jpg"
												alt="Mercedes-Benz S 600 Maybach S650 4 Seats*Panorama*Burmester">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz S 600 Maybach S650 4 Seats*Panorama*Burmester</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>463 kW (629 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 9.3l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>209 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- C -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>179.900 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>214.081 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17401_mercedes-benz-s-600-maybach-s650-4-seats-panorama-burmester">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17375_1.360x240.jpg"
												alt="Mercedes-Benz S 63 AMG 4M+ Lang*BRABUS S800* 800 PS*R21*Exhaust">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz S 63 AMG 4M+ Lang*BRABUS S800* 800 PS*R21*Exhaust</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>588 kW (800 PS)</span></p>
								<p>km-Stand:<span>50</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 8.9l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>203 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- D -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>230.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>273.700 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17375_mercedes-benz-s-63-amg-4m-lang-brabus-s800-800-ps-r21-exhaust">Weitere
									Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17423_1.360x240.jpg"
												alt="Mercedes-Benz S 63 AMG Cabrio 612 PS*Exklusiv*Ceramic*Burm. 3D">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz S 63 AMG Cabrio 612 PS*Exklusiv*Ceramic*Burm. 3D</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>450 kW (612 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 10.1l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>229 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- D -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>199.000 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>236.810 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17423_mercedes-benz-s-63-amg-cabrio-612-ps-exklusiv-ceramic-burm-3d">Weitere
									Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Mercedes-Benz Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17020_1.360x240.jpg"
												alt="Mercedes-Benz S 63 AMG Coupe 4M*Exclusive*Burmester*Head Up">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Mercedes-Benz S 63 AMG Coupe 4M*Exclusive*Burmester*Head Up</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>430 kW (585 PS)</span></p>
								<p>km-Stand:<span>20</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 10.3l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>242 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- F -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>142.900 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>170.051 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17020_mercedes-benz-s-63-amg-coupe-4m-exclusive-burmester-head-up">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Porsche Benzin">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17393_1.360x240.jpg"
												alt="Porsche 991 Turbo S Exclusive Series*Limited Edition">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Porsche 991 Turbo S Exclusive Series*Limited Edition</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>0 kW (0 PS)</span></p>
								<p>km-Stand:<span>50</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 9.1l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>212 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- F -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>349.000 €</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17393_porsche-991-turbo-s-exclusive-series-limited-edition">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Porsche Hybrid">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17115_1.360x240.jpg"
												alt="Porsche Panamera Turbo S*Ceramic*Carbon*Panorama*STOCK">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Porsche Panamera Turbo S*Ceramic*Carbon*Panorama*STOCK</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>500 kW (680 PS)</span></p>
								<p>km-Stand:<span>30</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Hybrid | 2.9l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>66 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- A+ -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>170.900 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>203.371 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17115_porsche-panamera-turbo-s-ceramic-carbon-panorama-stock">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Tesla Elekro">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17200_1.360x240.jpg"
												alt="Tesla Model S P100D*770PS*R21*Premium Sound*Schiebedach">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Tesla Model S P100D*770PS*R21*Premium Sound*Schiebedach</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>566 kW (770 PS)</span></p>
								<p>km-Stand:<span>50</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Elekro | 0l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>0 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!--  -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>139.500 € (netto)</p>
								</div>
								<div class="prodGrid__item-priceOld">
									<p>166.005 € (brutto)</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17200_tesla-model-s-p100d-770ps-r21-premium-sound-schiebedach">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-12 Toyota Benzin Gepanzert">
					<div class="prodGrid__item">
						<div class="prodGrid__item-img">
							<img src="/wp-content/themes/grandex/img/catalog/17221_1.360x240.jpg"
												alt="Toyota Land Cruiser 200 4.6 Petrol*ARMORED B6">

						</div>
						<div class="prodGrid__itemContent">
							<div class="prodGrid__item-title">
								<h2>Toyota Land Cruiser 200 4.6 Petrol*ARMORED B6</h2>
							</div>
							<div class="prodGrid__item-desc">
								<p>Leistung:<span>227 kW (309 PS)</span></p>
								<p>km-Stand:<span>50</span></p>
								<p>Status:<span>Neuwagen</span></p>
								<br>
								<div class="sidSmall">
									<p>Verbrauch:<span>Benzin | 12.9l / 100 km komb. *</span></p>
									<p>CO2 Emission:<span>403 g/km*</span></p>
									<p>Energieeffizienzklasse:<span><img
												src="http://byket.alex-bonum.ru/wp-content/themes/grandex/img/class_enegry.png" alt="class_enegry"></span>
									</p>
									<!-- G -->
								</div>
							</div>
							<br>
							<div class="prodGrid__item-price">
								<div class="prodGrid__item-priceNew">
									<p>97.900 €</p>
								</div>

							</div>
							<div class="prodGrid__item-btn">
								<a href="/FAHRZEUGE/17221_toyota-land-cruiser-200-4-6-petrol-armored-b6">Weitere Informationen</a>
							</div>
						</div>
					</div>
				</div>


			</div>
		</div>
	</section>
	<!-- END_PRODUCT_GRID_SIDASH -->

<?php get_footer(); ?>